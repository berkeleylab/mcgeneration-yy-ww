export PS1="\u@\H:\W> "
if ! [ -z "${SHIFTER_IMAGEREQUEST}" ]; then
  imageStr=`echo ${SHIFTER_IMAGEREQUEST} | rev | cut -d '/' -f 1 | rev`
  export PS1="${imageStr}:\W> "
fi

alias ll='ls -ltrh --color=tty'

lsetup git
asetup AthGeneration,21.6.99
