# MC Generation Studies for yy processes

## Setup

## Run Event Generation

## Runing with PyTaskFarmer

The PyTaskFarmer repository is: https://gitlab.cern.ch/berkeleylab/pytaskfarmer/-/tree/master
Detailed documentation is here: https://pytaskfarmer.readthedocs.io/en/latest/

For installation procedure, follow the instructions here: https://pytaskfarmer.readthedocs.io/en/latest/#quick-start

To setup a runner, detailed instructions are here https://pytaskfarmer.readthedocs.io/en/latest/runners.html#runners

Example of a runner file: `~/.pytaskfarmer/runners.d/atlas.ini`

```sh
[atlas]
Runner: taskfarmer.runners.ShifterRunner
image: zlmarshall/atlas-grid-centos7:20221021
module: cvmfs
setup: source ${HOME}/.bashrc && source /global/cfs/projectdirs/atlas/adimitri/BatchCoriScripts/pytaskfarmer/runatlascode.sh
```
Then the `runatlascode.sh` file contains details for setting up the Atlas relasese which should be used, for example:

```sh
shopt -s expand_aliases
source /global/cfs/cdirs/atlas/scripts/setupATLAS.sh
setupATLAS
asetup AthGeneration,21.6.99

#SOURCE and WORK should be defined here as well

```

Prepare a tasklist, one job per line. Example:
```sh
Gen_tf.py --ecmEnergy=13000. --firstEvent=1     --maxEvents=10000 --randomSeed=1234 --jobConfig=802301 --outputEVNTFile=official_file1_802301.EVNT.root
Gen_tf.py --ecmEnergy=13000. --firstEvent=10001 --maxEvents=20000 --randomSeed=1234 --jobConfig=802301 --outputEVNTFile=official_file2_802301.EVNT.root
Gen_tf.py --ecmEnergy=13000. --firstEvent=20001 --maxEvents=30000 --randomSeed=1234 --jobConfig=802301 --outputEVNTFile=official_file3_802301.EVNT.root
Gen_tf.py --ecmEnergy=13000. --firstEvent=30001 --maxEvents=40000 --randomSeed=1234 --jobConfig=802301 --outputEVNTFile=official_file4_802301.EVNT.root
Gen_tf.py --ecmEnergy=13000. --firstEvent=40001 --maxEvents=50000 --randomSeed=1234 --jobConfig=802301 --outputEVNTFile=official_file5_802301.EVNT.root

```


### Runing on interactive node

More details about interactive nodes on Cori/Perlmutter https://docs.nersc.gov/jobs/interactive/

To get a interactive node:
```sh
salloc --nodes 1 --qos interactive --time 01:00:00 --constraint haswell
```
adjust the time according to your job, maximum time is 4 hours.

Then just run the pytaskfarmer, for example
```sh
pytaskfarmer.py --r atlas --proc 4 --workdir myworkdir mywork.tasks
```


### Running on Batch

ToDo



## Plotting with Rivet

Full documentation: 
- https://gitlab.com/hepcedar/rivet/-/tree/release-3-1-x/
- https://rivet.hepforge.org/

Example analysis:
- https://rivet.hepforge.org/rivet-coverage
- https://rivet.hepforge.org/analyses/MC_FSPARTICLES.html
- https://rivet.hepforge.org/analyses/ATLAS_2019_I1734263.html

### Setup Rivet

To setup rivet everytime:
```sh
source setupRivet.sh
```
note: Athena relase should already be setup (`asetup AthGeneration,21.6.99`).

### Plotting

There is an example analysis in the repository: `RivetAnalysis_yy.cc`

To compile the analysis:

```sh
rivet-build RivetAnalysis_yy.so RivetAnalysis_yy.cc
```

now the library of the analysis should be created `RivetAnalysis_yy.so`

To run this analysis on the created EVNT files use `localRivetAnalysis_JO.py` script, please change the path to the file which should be analysed, then run:

```sh
athena localRivetAnalysis_JO.py
```

To produce plots:

```sh
rivet-mkhtml MyOutputPlots_RivetAnalysis_yy.cc.yoda.gz
```

default plots are pfds, to create png please run:

```sh
make-plots -f png rivet-plots/RivetAnalysis_yy/*dat
```


# Other Scripts

Inside `otherScripts` are a few examples of scripts for private MC production of samples to run simulation, reconstruction and derivation on GRID. Script makeSampleSRELinks.py will create soft links to samples which are already transfered to `NERSC_LOCALGROUPDISK`. More details are on the page: https://atlaswiki.lbl.gov/en/analysis/ExclusiveWW/MCPrivateProduction


# Event visualization
To visualize a few example events to debug what's going on, you can do the following:
- Create (or download) the EVNT file you want to inspect
- setting up a recent release (e.g. `21.0.137` or any recent 21.0 or 22.0 release, just make sure it's project `Athena`), edit the `jobOptions/dumpMC.py` accordingly to point to the input/output files and run it with `athena dumpMC.py`. It will output an HepMC file. If you want to select a particular event, just call athena as e.g. `athena --skipEvents=10 --maxEvents=1` to select the 10th event of the file.
- use the `hepmc2dot` modified package to convert this into a graph and make a PDF. The script `create-graph-pdf.sh` takes as argument the HepMC files (with expected .ascii extension) and converts all the way to a PDF with one event per page
