#!/usr/bin/env python

#run: python makeSampleSRELinks.py -o output_dir 'user.adimitri.mc16_13TeV.*s3804_r12425_p4396' 

##############################################################
#                                                            #
# makeSamplesRSELinks.py                                     # 
# S. Pagan Griso, 2016                                       #
#                                                            #
# Script for creating soft links to the files in a given     #
# dataset pointing to the local copy in the RSE              #
#                                                            #
# Parts are copy-paste from                                  #
# /afs/cern.ch/work/c/cohm/public/BGF/AnalysisBase-2.4.X/ \  #
# SUSYTools/scripts/CheckSampleStatus.py                     #
##############################################################

import sys, os, argparse, subprocess, shutil

def runCommand(cmd, verbose = False):
    if verbose:
        print "  Will run the following command: %s" % cmd
    cmdResult = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    return cmdResult.stdout

def findDids(pattern, verbose=False):
    cmd = 'rucio ls --short --filter type=CONTAINER %s' % pattern
    queryResult = runCommand(cmd, verbose)
    localListDids=[]
    for rawDid in queryResult.readlines():
        #strip scope name
        did = rawDid[rawDid.find(':')+1:]
        if did[-1]=='\n':
            did = did[:-1]
        localListDids.append(did)
    return localListDids

def main():
    #Setup command-line
    parser = argparse.ArgumentParser(description='Create soft links to location of dataset in local RSE (Dataset should have already been copied there)')
    parser.add_argument('dids', metavar='did', type=str, nargs='+', help='Dataset identifier')
    parser.add_argument('-s', '--srm', type=str, default='NERSC_LOCALGROUPDISK', help='Local RSE endpoint')
    parser.add_argument('-v', '--verbose', action='store_true', default=False, help='Verbose mode, more detailed output and commands, etc')
    parser.add_argument('-o', '--output', type=str, default='./', help='Output folder where datasets links will be created (for each dataset a folder with the dataset name will already be created automatically)')
    parser.add_argument('-r', '--resolve', default=False, action='store_true', help='Resolve dids as patterns through rucio ls')
    parser.add_argument('-f', '--fast', default=False, action='store_true', help='Skip datasets that already have an existing folder (won\'t check if all files are there!')
    args = parser.parse_args()
    if args.verbose:
        print args

    #Get list of dataset ids
    listdids=[]
    if (args.resolve):
        print('Resolving input dids patterns')
        for pattern in args.dids:
            listdids.extend(findDids(pattern, args.verbose))
    else:
        listdids = args.dids
    if args.verbose:
        print('List of datasets:')
        print(listdids)

    #Loop over datasets provided
    print('Creating soft links')
    listOfFiles={}
    if not args.verbose:
        print('DID -- nFiles')
    for did in listdids:
        outputDir = args.output + '/' + did + '/'
        #Check if output folder exists already
        if (os.path.exists(outputDir) and args.fast):
            continue #folder already exists, skip to next one
        #Get list of files in local RSE
        listOfFiles[did]=[]
        cmd = 'rucio list-file-replicas --rse %s %s' % (args.srm, did)
        raw_lof = runCommand(cmd, args.verbose)
        nLine=0        
        for line in raw_lof.readlines():
            nLine = nLine + 1
            if line.startswith('+--'):
                continue
            if line.startswith('|--'):
                continue
            if line.startswith('| SCOPE'):
                continue
            lfields = line.split('|')
            if len(lfields) < 7:
                print('Invalid rucio list-file-replicas output line (%d) for dataset: %s' % (nLine, did));
                if (args.verbose):
                    print line
                continue
            fileLocation=lfields[5][lfields[5].find(':')+1:].strip()
            #print "line here 2"
            #print fileLocation
            fileLocation=fileLocation[fileLocation.find('SFN=')+4:].strip()
            #print "line here 3"
            #print fileLocation
            if (fileLocation == ''):
                print('Invalid rucio list-file-replicas output line (%d) for dataset: %s' % (nLine, did));
                if (args.verbose):
                    print line
                continue
            #print "line here 4"
            #print fileLocation
            #Need, and assume, loca file, so strip-off anything else
            if fileLocation.startswith('ftp://'):
                #for things as ftp://server.org/file, only retain '/file'
                fileLocation = '/' + 'global/' + '/'.join(fileLocation.split('/')[3:])
                #print fileLocation
            if fileLocation.startswith('ps://'):
                #for things as https://server.org/file, only retain '/file'
                fileLocation = '/' + '/'.join(fileLocation.split('/')[3:])
                #print fileLocation

            #Now append to the list
            listOfFiles[did].append(fileLocation)

        if len(listOfFiles[did]) == 0:
            if args.verbose:
                print('No valid files found for dataset: %s' % did)
        #Create output directory
        if not os.path.exists(args.output):
            os.makedirs(args.output)
        if os.path.exists(outputDir):
            print('WARNING: output directory for dataset %s overwritten.' % did)
            shutil.rmtree(outputDir)
        if args.verbose:
            print('Creating dir: %s' % outputDir)
        os.makedirs(outputDir)

        #Create soft-links
        for oneFile in listOfFiles[did]:
            cmd = 'ln -s %s %s' % (oneFile, outputDir)
            runCommand(cmd, args.verbose)

        #Print stat
        if not args.verbose:
            print('%60s %5d' % (did, len(listOfFiles[did])))

    #Print general stats
    if args.verbose:
        print('%60s %5s' % ('DID','nFiles'))
        for did in listdids:
            print('%60s %5d' % (did, len(listOfFiles[did])))


if __name__ == '__main__':
    main()
