#!/bin/bash

#This script will submit a job to run simulation on grid, number of events per job and total number of events should be adjusted according to your sample
#GetTfCommand.py --AMI s3482
#asetup Athena,21.0.97

#To run: sh run_sim_s3482.sh input_file output_file


if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters, first argument is the input file and the second is the output file"
    exit 2
fi

pathena \
  --trf \
    'Sim_tf.py \
      --physicsList "FTFP_BERT_ATL_VALIDATION" \
      --truthStrategy "MC15aPlus" \
      --simulator "FullG4" \
      --postInclude "default:RecJobTransforms/UseFrontier.py" \
      --DBRelease "all:current" \
      --conditionsTag "default:OFLCOND-MC16-SDR-14" \
      --DataRunNumber "284500" \
      --preExec "EVNTtoHITS:simFlags.SimBarcodeOffset.set_Value_and_Lock(200000)" "EVNTtoHITS:simFlags.TRTRangeCut=30.0;simFlags.TightMuonStepping=True" \
      --preInclude "EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py,SimulationJobOptions/preInclude.FrozenShowersFCalOnly.py" \
      --geometryVersion "default:ATLAS-R2-2016-01-00-01_VALIDATION" \
      --postExec "all:from IOVDbSvc.CondDB import conddb;conddb.addOverride(\"/Indet/Beampos\",\"IndetBeampos-13TeV-MC16-002\")" \
      --AMITag "s3482" \
      --maxEvents 500 \
      --skipEvents %SKIPEVENTS \
      --inputEVNTFile "%IN" \
      --outputHITSFile "%OUT.HITS.root" '\
  --inDS $1 \
  --outDS $2 \
  --nEventsPerJob 500 --nEventsPerFile 20000 \
  --nFiles 1



