#!/bin/bash

#This script will submit a job to run simulation on grid, number of events per job and total number of events should be adjusted according to your sample
#GetTfCommand.py --AMI p4398
#asetup AthDerivation,21.2.116.0 

#To run: sh run_deriv_p4398.sh input_file output_file

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters, first argument is the input file and the second is the output file"
    exit 2
fi

pathena \
  --trf \
    'Reco_tf.py \
      --reductionConf "EGAM1" \
      --passThrough "True" \
      --sharedWriter "True" \
      --athenaMPMergeTargetSize "DAOD_*:0" \
      --preExec "default:from BTagging.BTaggingFlags import BTaggingFlags;BTaggingFlags.CalibrationTag = \"BTagCalibRUN12-08-49\"; from AthenaCommon.AlgSequence import AlgSequence; topSequence = AlgSequence(); topSequence += CfgMgr.xAODMaker__DynVarFixerAlg(\"InDetTrackParticlesFixer\", Containers = [ \"InDetTrackParticlesAux.\" ] );from AthenaMP.AthenaMPFlags import jobproperties as ampjp;ampjp.AthenaMPFlags.UseSharedWriter=True;import AthenaPoolCnvSvc.AthenaPool;ServiceMgr.AthenaPoolCnvSvc.OutputMetadataContainer=\"MetaData\";topSequence += CfgMgr.xAODMaker__DynVarFixerAlg(\"BTaggingELFixer\", Containers = [\"BTagging_AntiKt4EMTopoAux.\" ] );" \
      --inputAODFile "%IN" \
      --outputDAODFile "%OUT.pool.root" \
      --AMITag "p4398" '\
  --inDS $1 \
  --outDS $2 \
  --destSE NERSC_LOCALGROUPDISK





