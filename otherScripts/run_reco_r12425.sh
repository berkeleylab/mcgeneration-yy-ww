#!/bin/bash

#This script will submit a job to run simulation on grid, number of events per job and total number of events should be adjusted according to your sample
#GetTfCommand.py --AMI r12425
#asetup Athena,21.0.121
#Input file arguments:
# --inputLowPtMinbiasHitsFile 'myLowPtMinbiasHits' --inputHighPtMinbiasHitsFile 'myHighPtMinbiasHits'
#mc16_13TeV.361239.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_high.merge.HITS.e4981_s3087_s3437_s3503
#mc16_13TeV.361238.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_low.merge.HITS.e4981_s3087_s3437_s3503

#To run: sh run_reco_r12425.sh  input_file output_file

if [ "$#" -ne 2 ]; then
    echo "Illegal number of parameters, first argument is the input file and the second is the output file"
    exit 2
fi

pathena \
  --trf \
    'Reco_tf.py \
      --AMITag "r12425" \
      --numberOfHighPtMinBias "0.2595392" \
      --digiSteeringConf "StandardSignalOnlyTruth" \
      --conditionsTag "default:OFLCOND-MC16-SDR-25" \
      --valid "True" \
      --steering "doRDO_TRIG" \
      --pileupFinalBunch "6" \
      --asetup "RDOtoRDOTrigger:Athena,21.0.77" \
      --autoConfiguration "everything" \
      --numberOfLowPtMinBias "99.2404608" \
      --geometryVersion "default:ATLAS-R2-2016-01-00-01" \
      --preInclude "HITtoRDO:Digitization/ForceUseOfPileUpTools.py,SimulationJobOptions/preInlcude.PileUpBunchTrainsMC16c_2017_Config1.py,RunDependentSimData/configLumi_run310000.py" \
      --postExec "all:CfgMgr.MessageSvc().setError+=[\"HepMcParticleLink\"]" "ESDtoAOD:fixedAttrib=[s if \"CONTAINER_SPLITLEVEL = \\\"99\\\"\" not in s else \"\" for s in svcMgr.AthenaPoolCnvSvc.PoolAttributes];svcMgr.AthenaPoolCnvSvc.PoolAttributes=fixedAttrib" "all:from IOVDbSvc.CondDB import conddb;conddb.addOverride(\"/Indet/Beampos\",\"IndetBeampos-13TeV-MC16-002\")" "HITtoRDO:streamRDO.ItemList.remove(\"xAOD::EventInfo#*\"); streamRDO.ItemList.remove(\"xAOD::EventAuxInfo#*\");" \
      --preExec "all:rec.Commissioning.set_Value_and_Lock(True);from AthenaCommon.BeamFlags import jobproperties;jobproperties.Beam.numberOfCollisions.set_Value_and_Lock(20.0);from LArROD.LArRODFlags import larRODFlags;larRODFlags.NumberOfCollisions.set_Value_and_Lock(20);larRODFlags.nSamples.set_Value_and_Lock(4);larRODFlags.doOFCPileupOptimization.set_Value_and_Lock(True);larRODFlags.firstSample.set_Value_and_Lock(0);larRODFlags.useHighestGainAutoCorr.set_Value_and_Lock(True); from LArDigitization.LArDigitizationFlags import jobproperties;jobproperties.LArDigitizationFlags.useEmecIwHighGain.set_Value_and_Lock(False)" "ESDtoAOD:from TriggerJobOpts.TriggerFlags import TriggerFlags;TriggerFlags.AODEDMSet.set_Value_and_Lock(\"AODSLIM\");" "RAWtoESD:from InDetRecExample.InDetJobProperties import InDetFlags; InDetFlags.doLowPtRoI.set_Value_and_Lock(True); InDetFlags.LowPtRoIStrategy.set_Value_and_Lock(2);InDetFlags.LowPtRoIWindow.set_Value_and_Lock(-1);" \
      --triggerConfig "RDOtoRDOTrigger=MCRECO:DBF:TRIGGERDBMC:2216,76,260" \
      --numberOfCavernBkg "0" \
      --postInclude "default:PyJobTransforms/UseFrontier.py" \
      --maxEvents 500 \
      --skipEvents %SKIPEVENTS \
      --jobNumber %RNDM:40 \
      --runNumber 505485 \
      --digiSeedOffset1 %RNDM:40 --digiSeedOffset2 %RNDM:40 \
      --inputLowPtMinbiasHitsFile "%LOMBIN" \
      --inputHighPtMinbiasHitsFile "%HIMBIN" \
      --inputHITSFile "%IN" \
      --outputAODFile "%OUT.mc16e.AOD.pool.root" '\
  --lowMinDS "mc16_13TeV:mc16_13TeV.361238.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_low.merge.HITS.e4981_s3087_s3437_s3503" \
  --highMinDS "mc16_13TeV:mc16_13TeV.361239.Pythia8EvtGen_A3NNPDF23LO_minbias_inelastic_high.merge.HITS.e4981_s3087_s3437_s3503" \
  --inDS $1 \
  --outDS $2 \
  --nEventsPerJob 500 --nEventsPerFile 500 \
  --nFiles 40 --nLowMin 7 --nHighMin 5


