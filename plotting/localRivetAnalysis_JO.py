theApp.EvtMax = -1

import AthenaPoolCnvSvc.ReadAthenaPool
svcMgr.EventSelector.InputCollections = [ '/global/cfs/projectdirs/atlas/adimitri/MC_request/Superchic_4.14/rel21.6_LHE/official_802301.EVNT.root' ]

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from Rivet_i.Rivet_iConf import Rivet_i
rivet = Rivet_i()
import os
rivet.AnalysisPath = os.environ['PWD']

rivet.Analyses += [ 'RivetAnalysis_yy' ]
rivet.RunName = ''
rivet.HistoFile = 'MyOutputPlots_RivetAnalysis_yy.cc.yoda.gz'
rivet.CrossSection = 1.0
#rivet.IgnoreBeamCheck = True
job += rivet

